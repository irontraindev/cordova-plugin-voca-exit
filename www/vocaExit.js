// iOS 인 경우 app 메소드가 없는 것으로 판단하였습니다.
if ('navigator' in window && !('app' in window.navigator)){

    var argscheck = require('cordova/argscheck'),
        utils = require('cordova/utils'),
        exec = require('cordova/exec');
        /**
         * 안드로이드와 같은 형태로 사용하기 위해
         * navigator.app.exitApp과 같은 경로로 종료 함수 적용
         * usage> navigator.app.exitApp();
         */
    var VocaExit = new Object();

        window.navigator.app = {
          exitApp:function(askUser){
            if(!askUser) {
              askUser = false;
            }

            exec(
              function callback(data) {
                console.log("callback Message");
              },
              function errorHandler(err) {
                console.log("Error" + err);
              },
              'VocaExit',
              'exitApp',
              [askUser]
            );
          }
        };
        
        module.exports = VocaExit = window.navigator.app;
}