//
//  cordova-plugin-voca-exit
//  ios 전용 앱 종료 플러그인
//

#import <Cordova/CDV.h>

@interface CDVVocaExit : CDVPlugin

- (void)exitApp:(CDVInvokedUrlCommand*)command;

@end
